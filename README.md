# Benchmark Permissioned Blockchain-based Assembly Device Feature Management System (FMS)

Benchmark permission blockchain based FMS prototype,The program benchmarks performance of license smart contract that specified in the chaincode folder, the prototype is build upon Hyperleger Fabric with following setup: 
* 2 organizations, 
* 2 peers nodes for each organization, 
* 3 ectd nodes cluster for ordering service, 
* A couchDB node for each peer node, 
* One CA node for each organization, 

## File Structure
```
.
├── benchmarks   // contains different benchmarks configuration files
├── chaincode    // contains smart contracts that to be test
├── networks     // blockchain network configuration files
├── package.json // dependcies
├── logScripts   // for debugging, attach docker containers output to stdout
└── report       // reports
```

## Install caliper
```
npm install
npx caliper bind \
    --caliper-bind-sut fabric \
    --caliper-bind-sdk 1.4.4

```
## Run Benchmark
e.g.
```
npx caliper benchmark run --caliper-benchconfig benchmarks/config_example.yaml --caliper-networkconfig networks/2org2peercouchdb_raft/fabric-node-tls.yaml
npx caliper benchmark run --caliper-benchconfig benchmarks/config_issue_transfer_fixedbacklog.yaml --caliper-networkconfig networks/2org2peercouchdb_raft/fabric-node-tls.yaml
npx caliper benchmark run --caliper-benchconfig benchmarks/config_transfer_fixedRate.yaml --caliper-networkconfig networks/2org2peercouchdb_raft/fabric-node-tls.yaml
```

## Reports
Reports can be find in the report folder
```
report
├── cpu_chart.py
├── cpu_consumption.png
├── mem_chart.py
├── memory_consumption.png
├── report_query_issue_transfer_fixedbacklog.html
├── report_transfer_fixedRate.html
├── TransferLicenseFailRate.png
└── trans_performace_chart.py

```
report_query_issue_transfer_fixedbacklog.html show the results of maximum possible sending rate the result can be summarized to the following table:

Name	          |Succ  |Fail  |Send Rate (TPS)  |Max Latency (s)  |Min Latency (s)  |Avg Latency (s)  |Throughput (TPS)
------------------|------|------|-----------------|-----------------|-----------------|-----------------|--------------
QueryAllLicenses  | 1000 | 0    | 87.3            | 0.47            | 0.03            | 0.19            | 86.4
QueryOwnLicenses  | 1000 | 0    | 191.6           | 0.21            | 0.02            | 0.09            | 190.2
TransferLicenses  | 144  | 856  | 61.6            | 0.70            | 0.19            | 0.49            | 8.6
IssueLicense      | 1000 | 0    | 31.8            | 0.82            | 0.08            | 0.49            | 31.4


__Resource consumption charts:__

![Memory consumption](./report/memory_consumption.png)
![CPU% consumption](./report/cpu_consumption.png)

The transferLicenses has extreme high failing rate, as the transferLicenses is the bottle neck for the system, we test it further with different sending rates and the results are as following (see details in report_transfer_fixedRate.html):

 Name             | Succ | Fail | Send Rate (TPS) | Max Latency (s) | Min Latency (s) | Avg Latency (s) | Throughput (TPS)
------------------|------|------|-----------------|-----------------|-----------------|-----------------|-----------------
 transferLicenses | 1000 | 0    | 7.0             | 0.75            | 0.59            | 0.66            | 7.0
 transferLicenses | 940  | 60   | 8.0             | 0.73            | 0.56            | 0.62            | 7.5
 transferLicenses | 511  | 489  | 9.0             | 0.68            | 0.57            | 0.64            | 4.6
 transferLicenses | 546  | 454  | 10.1            | 0.71            | 0.13            | 0.57            | 5.5
 transferLicenses | 495  | 505  | 15.1            | 0.78            | 0.29            | 0.65            | 7.4
 transferLicenses | 435  | 565  | 20.1            | 0.76            | 0.37            | 0.55            | 8.7
 transferLicenses | 335  | 665  | 25.1            | 0.80            | 0.48            | 0.56            | 8.3
 transferLicenses | 300  | 700  | 30.1            | 0.74            | 0.39            | 0.50            | 8.9
 transferLicenses | 250  | 750  | 35.2            | 0.73            | 0.41            | 0.55            | 8.6
 transferLicenses | 225  | 775  | 40.2            | 0.65            | 0.41            | 0.55            | 8.8
 transferLicenses | 200  | 800  | 45.2            | 0.70            | 0.43            | 0.53            | 8.8
 transferLicenses | 185  | 815  | 50.2            | 0.66            | 0.43            | 0.55            | 9.0
 transferLicenses | 165  | 835  | 55.3            | 0.69            | 0.39            | 0.55            | 8.9
 transferLicenses | 155  | 845  | 60.3            | 0.72            | 0.39            | 0.55            | 9.1

__Transfer failing rate charts:__

![transferLicenses failing rates](./report/TransferLicenseFailRate.png)

To re-generate charts:
```
cd report
python3 cpu_chart.py
python3 mem_chart.py
python3 trans_performace_chart.py
```

## Reference
1. [Caliper benchmark configuration](https://hyperledger.github.io/caliper/vLatest/bench-config/)
2. [Caliper fabric network configuration](https://hyperledger.github.io/caliper/vLatest/fabric-config/)
3. [Caplier benchmark examples](https://github.com/hyperledger/caliper-benchmarks)
