echo Stop and rm on all log monitoring containers on the network

docker kill logspout 2> /dev/null 1>&2 || true
docker rm logspout 2> /dev/null 1>&2 || true
