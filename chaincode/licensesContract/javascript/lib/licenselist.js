/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Utility class for collections of ledger states --  a state list
const StateList = require('./../ledger-api/statelist.js');

const License = require('./license.js');

class LicenseList extends StateList {

    constructor(ctx) {
        super(ctx, 'org.fmsnet.licenselist');
        this.use(License);
    }

    async addLicense(license) {
        return await this.addState(license);
    }

    async getLicense(org, owner, licenseId) {
        let key = [org, owner, licenseId];
        //TODO: figure out why key is 2-D array [[ower, licenseId], undefined] instead of [owner, licenseId]
        return await this.getState(key[0]);
    }

    async updateLicense(license) {
        return await this.addLicense(license);
    }

    async getLicensesFrom(org, owner) {
        return await this.getStatesFromTwoKeys(org, owner);
    }

    async getAllLicenses(org) {
        return await this.getStatesFromKey(org);
    }

}


module.exports = LicenseList;
