/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Fabric smart contract classes
const { Contract, Context } = require('fabric-contract-api');
const ClientIdentity = require('fabric-shim').ClientIdentity;

// PaperNet specifc classes
const License = require('./license.js');
const LicenseList = require('./licenselist.js');

/**
 * A custom context provides easy access to list of all license
 */
class LicenseContext extends Context {

    constructor() {
        super();
        // All papers are held in a list of papers
        this.licenseList= new LicenseList(this);
    }

}

/**
 * Define commercial paper smart contract by extending Fabric Contract class
 *
 */
class LicenseContract extends Contract {

    constructor() {
        // Unique name when multiple contracts per chaincode file
        super('org.fmsnet.licensecontract');
    }

    init() {}

    /**
     * Define a custom context for fms
     */
    createContext() {
        return new LicenseContext();
    }

    /**
     * Instantiate to perform any setup of the ledger that might be required.
     * @param {Context} ctx the transaction context
     */
    async initLedger(ctx) {
        console.info('============= Start : Initialize Ledger ===========');
    }

    /**
     * Issue License
     *
     * @param {Context} ctx the transaction context
     * @param {String} licenseId unique license identity
     * @param {integer} count the number of the license
     * @param {String} name license name
     * @param {String} expiredate license expiredate
     * @param {String} org new license owner's org presentation
     * @param {String} owner new license owner
     * @param {String} content license features that encrypt by owner (pub key)
    */
    async issue(ctx, licenseId, name, count, expiredate, org, owner,  content) {
        console.info('============= Issue License ===========');
        let license = License.createInstance(licenseId, name, count, expiredate, org, owner, content);
        license.setIssued();
        await ctx.licenseList.addLicense(license);
        console.info('Issue license <--> ', license.toBuffer());

        // Must return a serialized paper to caller of smart contract
        return license.toBuffer();
    }

    /**
     * Transfer License
     *
     * @param {Context} ctx the transaction context
     * @param {String} newId unique new license identity
     * @param {integer} count the number of the license
     * @param {String} expiredate license expiredate
     * @param {String} newOwner new license owner
     * @param {String} newOrg new license owner's org presentation
     * @param {String} content license features that encrypt by owner (pub key)
    */
    async transfer(ctx, oldId, newId, count, newOrg, newOwner, content) {
        console.info('============= Transfer License ===========');

        const cid = new ClientIdentity(ctx.stub);
        const oldOwner= Buffer.from(cid.getID()).toString('base64');

        const  oldOrg = cid.getMSPID();
        console.info('owners pubaddr: ' + oldOwner);

        let license = await ctx.licenseList.getLicense([oldOrg, oldOwner, oldId]);

        // Validate current owner
        if (oldOwner === newOwner) {
            return license;
        }

        // Retrieve the current paper using key fields provided
        const license_name = license.getName();
        const license_exp = license.getExpireDate();

        // If paper is not in the issue state throw expection
        if (!license.isIssued()) {
            throw new Error('License ' + oldId + ' is Invalid ');
        }

        if (!this.isNotExpired(license_exp)) {
            license.setRedeemed();
            await ctx.licenseList.updateLicense(license);
            throw new Error('License Expired');
        }

        const existing_count = license.getLicenseCount();
        if (existing_count > count) {
            // split existing license instance into two
            license.update(existing_count-count, oldOwner, license.getContent());
            await ctx.licenseList.updateLicense(license);

            let newlicense = License.createInstance(newId, license_name, count, license_exp, newOrg, newOwner, content);
            newlicense.setIssued();
            await ctx.licenseList.addLicense(newlicense);
        } else if (existing_count === count) {
            license.update(count, newOwner, content);
            await ctx.licenseList.updateLicense(license);
        } else {
            throw new Error('License ' + oldId + ' do not have enough lincense count');
        }
        return license.toBuffer();
    }

    /** get all licenses from specific owner
      * @param {Context} ctx the transaction context
      * @param {String} owner existing license owner
      */
    async getOwnLicenses(ctx) {
        const cid = new ClientIdentity(ctx.stub);
        const  owner= Buffer.from(cid.getID()).toString('base64');
        const  org = cid.getMSPID();
        console.info('=========== Inquire License from ' + org + owner);
        let iter = await ctx.licenseList.getLicensesFrom(org, owner);
        return this.getStatesFromIterator(iter, ctx);
    }

    /** get all licenses
      * @param {Context} ctx the transaction context
      */
    async getAllLicenses(ctx) {
        const cid = new ClientIdentity(ctx.stub);
        const  org = cid.getMSPID();
        console.info('========== List All Licenses from org ' + org);
        let iter = await ctx.licenseList.getAllLicenses(org);
        return this.getStatesFromIterator(iter, ctx);
    }

    isNotExpired(date) {
        const expiredate = new Date(date);
        const now = new Date();
        return expiredate > now;
    }

    /**
     * Deserialize a state data to license
     * @param {Buffer} data to form back into the object
     */
    async getStatesFromIterator(iterator,ctx) {
        const allResults = [];
        for (;;) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                let valStr = res.value.value.toString('utf8');
                console.log(valStr);
                let stateJSON = {};
                try {
                    stateJSON= JSON.parse(valStr);
                } catch (err) {
                    let jsonResp = {};
                    jsonResp.error = 'Failed to decode JSON from get states';
                    throw new Error(stateJSON);
                }
                if (stateJSON.currentState === 1) {
                    if(this.isNotExpired(stateJSON.licenseobject.expiredate)) {
                        allResults.push(stateJSON);
                    } else {
                        let license = License.createInstanceFromLicObj(stateJSON.licenseId, stateJSON.licenseobject);
                        license.setRedeemed();
                        await ctx.licenseList.updateLicense(license);
                    }
                }
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }
}

module.exports = LicenseContract;
