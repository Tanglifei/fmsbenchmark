/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

// Utility class for ledger state
const State = require('./../ledger-api/state.js');

// Enumerate License state values
const LicenseState = {
    ISSUED: 1,
    REDEEMED: 2
};
/**
 * License class extends State class
 * Class will be used by application and smart contract to define a License
 */
class License extends State {

    constructor(obj) {
        super(License.getClass(), [obj.licenseobject.org, obj.licenseobject.owner, obj.licenseId]);
        Object.assign(this, obj);
    }

    /**
     * Basic getters and setters
    */
    getContent() {
        return this.licenseobject.content;
    }

    getName() {
        return this.licenseobject.name;
    }

    getOwner() {
        return this.licenseobject.owner;
    }

    getExpireDate() {
        return this.licenseobject.expiredate;
    }

    getLicenseCount() {
        return this.licenseobject.count;
    }

    update(count, owner, content){
        this.licenseobject.count = count;
        this.licenseobject.owner = owner;
        this.licenseobject.content = content;
    }

    static fromBuffer(buffer) {
        return License.deserialize(buffer);
    }

    setIssued() {
        this.currentState = LicenseState.ISSUED;
    }

    setRedeemed() {
        this.currentState = LicenseState.REDEEMED;
    }

    isIssued() {
        return this.currentState === LicenseState.ISSUED;
    }

    isRedeemed() {
        return this.currentState === LicenseState.REDEEMED;
    }

    toBuffer() {
        return Buffer.from(JSON.stringify(this));
    }

    static deserialize(data) {
        return State.deserializeClass(data, License);
    }

    /**
     * Factory method to create a commercial paper object
     */
    static createInstance(licenseId, name, count, expiredate, org, owner, content) {
        return new License({licenseId, licenseobject: {name, count, expiredate, org, owner, content}});
    }

    static createInstanceFromLicObj(licenseId, licenseobject) {
        return new License({licenseId, licenseobject});
    }

    static getClass() {
        return 'org.fmsnet.license';
    }
}

module.exports = License;
