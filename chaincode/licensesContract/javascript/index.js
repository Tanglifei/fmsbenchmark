/*
SPDX-License-Identifier: Apache-2.0
*/

'use strict';

const LicenseContract = require('./lib/licensecontract.js');
module.exports.contracts = [LicenseContract];
