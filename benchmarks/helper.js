/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.Org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

const uuid = require('./uuid.js');

const TestLicense1 = {
    name: 'TestLicense1',
    count: '100000',
    expiredate : '2020-12-12',
    content: 'WwiZmVhdHVyZV8xIjp7Im5hbWUiOiJmZWF0dXJlXzEiLCJzdXBwb3J0ZWRfc3dfdmVyc2lvbiI6WyIxLjEuMSJdfSwKImZlYXR1cmVfMiI6eyJuYW1lIjoiZmVhdHVyZV8yIiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfMyI6eyJuYW1lIjoiZmVhdHVyZV8zIiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNCI6eyJuYW1lIjoiZmVhdHVyZV80Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNSI6eyJuYW1lIjoiZmVhdHVyZV81Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNiI6eyJuYW1lIjoiZmVhdHVyZV82Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNyI6eyJuYW1lIjoiZmVhdHVyZV83Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfOCI6eyJuYW1lIjoiZmVhdHVyZV84Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfOSI6eyJuYW1lIjoiZmVhdHVyZV85Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfMTAiOnsibmFtZSI6ImZlYXR1cmVfMTAiLCJzdXBwb3J0ZWRfc3dfdmVyc2lvbiI6WyIxLjEuMSIsIjEuMi4zIl19LAoiZmVhdHVyZV8xMSI6eyJuYW1lIjoiZmVhdHVyZV8xMSIsInN1cHBvcnRlZF9zd192ZXJzaW9uIjpbIjEuMS4xIiwiMS4yLjMiXX0KXQo='
};

const generateOwnerPubAddr = () => {
    return 'eDUwOTo6L0M9VVMvU1Q9Q2FsaWZvcm5pYS9MPVNhbiBGcmFuY2lzY28vQ049VXNlcjFAb3JnMS5leGFtcGxlLmNvbTo6L0M9VVMvU1Q9Q2FsaWZvcm5pYS9MPVNhbiBGcmFuY2lzY28vTz1vcmcxLmV4YW1wbGUuY29tL0NOPWNhLm9yZzEuZXhhbXBsZS5jb20=';
}

const generateRandomOwnerPubAddr = () => {
    return  Math.random().toString(36);
}

const issueLicensesForQueryAll = async function (bc, contx, args) {
    let assetIndex = 0;
    while (assetIndex< args.assets) {
        const id = uuid();
        const owner = generateRandomOwnerPubAddr();
        await issue(bc, contx, 'Org1MSP', owner, id);
        assetIndex++;
    }
};

const issueLicensesForQueryOwn = async function (bc, contx, args) {
    let assetIndex = 0;
    while (assetIndex< args.assets) {
        const id = uuid();
        let owner;
        if (assetIndex == 0) {
            owner = generateOwnerPubAddr();
        } else  {
            owner = generateRandomOwnerPubAddr();
        }
        await issue(bc, contx, 'Org1MSP', owner, id);
        assetIndex++;
    }
};

const issue = async function (bc, contx, org, owner, id) {
    let args = {
        chaincodeFunction: 'issue',
        chaincodeArguments: [id, TestLicense1.name, TestLicense1.count, TestLicense1.expiredate, org, owner, TestLicense1.content]
    };

    await bc.invokeSmartContract(contx, 'licensesContract', 'v0', args, 30);
};

const issueLicenseForTransfer = async function (bc, contx, args) {
    const id = uuid();
    const owner = generateOwnerPubAddr();
    await issue(bc, contx, 'Org1MSP', owner, id);
    return id;
};

module.exports = {TestLicense1, generateRandomOwnerPubAddr, issueLicensesForQueryAll, issueLicensesForQueryOwn, issueLicenseForTransfer};
