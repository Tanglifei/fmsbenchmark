/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

'use strict';

module.exports.info = 'Changing license owner.';

const uuid = require('./uuid.js');
const issueLicense = require('./helper').issueLicenseForTransfer;
const generateOwnerPubAddr = require('./helper').generateRandomOwnerPubAddr;
let bc, contx, id;

module.exports.init = async function(blockchain, context, args) {
    bc = blockchain;
    contx = context;
    id = await issueLicense(bc, contx, args);
    return Promise.resolve();
};

module.exports.run = function() {
    const newid = uuid();
    const count = '1';
    const org = 'Org1MSP';
    const newowner = generateOwnerPubAddr();
    const content = 'WwiZmVhdHVyZV8xIjp7Im5hbWUiOiJmZWF0dXJlXzEiLCJzdXBwb3J0ZWRfc3dfdmVyc2lvbiI6WyIxLjEuMSJdfSwKImZlYXR1cmVfMiI6eyJuYW1lIjoiZmVhdHVyZV8yIiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfMyI6eyJuYW1lIjoiZmVhdHVyZV8zIiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNCI6eyJuYW1lIjoiZmVhdHVyZV80Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNSI6eyJuYW1lIjoiZmVhdHVyZV81Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNiI6eyJuYW1lIjoiZmVhdHVyZV82Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfNyI6eyJuYW1lIjoiZmVhdHVyZV83Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfOCI6eyJuYW1lIjoiZmVhdHVyZV84Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfOSI6eyJuYW1lIjoiZmVhdHVyZV85Iiwic3VwcG9ydGVkX3N3X3ZlcnNpb24iOlsiMS4xLjEiLCIxLjIuMyJdfSwKImZlYXR1cmVfMTAiOnsibmFtZSI6ImZlYXR1cmVfMTAiLCJzdXBwb3J0ZWRfc3dfdmVyc2lvbiI6WyIxLjEuMSIsIjEuMi4zIl19LAoiZmVhdHVyZV8xMSI6eyJuYW1lIjoiZmVhdHVyZV8xMSIsInN1cHBvcnRlZF9zd192ZXJzaW9uIjpbIjEuMS4xIiwiMS4yLjMiXX0KXQo=';

    let args = {
        chaincodeFunction: 'transfer',
        chaincodeArguments: [id, newid, count, org, newowner, content]
    };
    return bc.invokeSmartContract(contx, 'licensesContract', 'v1', args, 30);
};

module.exports.end = async function() {
    return Promise.resolve();
};
