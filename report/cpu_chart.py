import numpy as np
import matplotlib.pyplot as plt

# data to plot
n_groups = 4
P0OrgS_Max = (71, 76, 64, 44)
P0OrgB_Max = (75, 77, 69, 43)
P1OrgB_Max = (73, 72, 67, 41)
P1OrgS_Max = (72, 73, 65, 50)
PeerMax= (75, 74, 69, 50)

P0OrgS_Avg = (39, 36, 46, 24)
P0OrgB_Avg = (41, 36, 46, 24)
P1OrgB_Avg = (40, 34, 46, 24)
P1OrgS_Avg = (41, 35, 46, 24)
PeerAvg= (40, 35, 46, 24)

Order2_Max = (4, 4, 17, 13)
Order1_Max = (4, 4, 15, 11)
Order0_Max = (1, 1, 11, 12)
OrderMax= (4, 3, 17, 12)

Order2_Avg = (1, 1, 11, 7)
Order1_Avg = (1, 2, 11, 6)
Order0_Avg = (1, 1, 6, 5)
OrderAvg= (1, 1.5, 9, 6)

CouchP1OrgS_Max = (94, 140, 33, 34)
CouchP0OrgS_Max = (103, 145, 34, 25)
CouchP1OrgB_Max = (95, 147, 34, 29)
CouchP0OrgB_Max = (99, 142, 32, 29)
CouchPeerMax= (103, 147, 34, 34)

CouchP1OrgS_Avg = (53, 63, 26, 16)
CouchP0OrgS_Avg = (52, 62, 26, 14)
CouchP1OrgB_Avg = (52, 61, 27, 15)
CouchP0OrgB_Avg = (50, 60, 24, 14)
CouchPeerAvg= (52, 61, 26, 15)

CAOrgS_Max = (0, 0, 0, 0)
CAOrgB_Max = (0, 0, 0, 0)
CAMax = (0, 0, 0, 0)

CAOrgS_Avg = (0, 0, 0, 0)
CAOrgB_Avg = (0, 0, 0, 0)
CAAvg = (0, 0, 0, 0)

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.20
opacity = 1

rects1 = plt.bar(index, PeerMax, bar_width,
alpha=opacity,
# color='b',
label='peer max')

rects2 = plt.bar(index + bar_width, OrderMax, bar_width,
alpha=opacity,
# color='g',
label='order max')

rects3 = plt.bar(index + bar_width*2, CouchPeerMax, bar_width,
alpha=opacity,
# color='g',
label='couchDB max')

rects4 = plt.bar(index + bar_width*3, CAMax, bar_width,
alpha=opacity,
# color='g',
label='CA max')

rects1_ = plt.bar(index, PeerAvg, bar_width,
alpha=opacity,
# color='b',
label='peer avg')

rects2_ = plt.bar(index + bar_width, OrderAvg, bar_width,
alpha=opacity,
# color='g',
label='order avg')

rects3_ = plt.bar(index + bar_width*2, CouchPeerAvg, bar_width,
alpha=opacity,
# color='g',
label='couchDB avg')

rects4_ = plt.bar(index + bar_width*3, CAAvg, bar_width,
alpha=opacity,
# color='g',
label='CA avg')

# plt.xlabel('M')
plt.ylabel('CPU%')
# plt.title('Scores by person')
plt.xticks(index + bar_width, ('QueryAll', 'QueryOwn', 'Transfer', 'Issue'))
plt.legend()

plt.tight_layout()
plt.show()
