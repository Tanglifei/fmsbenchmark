import numpy as np
import matplotlib.pyplot as plt

# data to plot
n_groups = 4
P0OrgS_Max = (360, 361, 378, 393)
P0OrgB_Max = (290, 291, 309, 342)
P1OrgB_Max = (398, 398, 417, 430)
P1OrgS_Max = (392, 393, 411, 423)
PeerMax= (398, 398, 417, 430)

Order2_Max = (27, 29, 141, 223)
Order1_Max = (27, 29, 136, 212)
Order0_Max = (25, 27, 126, 199)
OrderMax= (27, 29, 141, 223)

CouchP1OrgS_Max = (84, 84, 85, 87)
CouchP0OrgS_Max = (81, 89, 80, 84)
CouchP1OrgB_Max = (83, 84, 83, 87)
CouchP0OrgB_Max = (82, 84, 81, 85)
CouchPeerMax= (84, 89, 85, 87)

CAOrgS_Max = (9, 9, 9, 9)
CAOrgB_Max = (9, 9, 9, 9)
CAMax = (9, 9, 9, 9)


P0OrgS_Avg = (305, 360, 369, 385)
P0OrgB_Avg = (269, 291, 300, 322)
P1OrgB_Avg = (339, 398, 407, 423)
P1OrgS_Avg = (337, 393, 402, 417)
PeerAvg= (312, 360, 369, 386)

Order2_Avg = (25, 29, 88, 192)
Order1_Avg = (26, 29, 89, 182)
Order0_Avg = (24, 27, 82, 169)
OrderAvg= (25, 28, 86, 181)

CouchP1OrgS_Avg = (73, 82, 84, 87)
CouchP0OrgS_Avg = (73, 82, 79, 84)
CouchP1OrgB_Avg = (73, 81, 83, 87)
CouchP0OrgB_Avg = (72, 81, 80, 85)
CouchPeerAvg= (73, 82, 81, 87)

CAOrgS_Avg = (9, 9, 9, 9)
CAOrgB_Avg = (9, 9, 9, 9)
CAAvg = (9, 9, 9, 9)

# create plot
fig, ax = plt.subplots()
index = np.arange(n_groups)
bar_width = 0.20
opacity = 1

rects1 = plt.bar(index, PeerMax, bar_width,
alpha=opacity,
# color='b',
label='peer max')

rects2 = plt.bar(index + bar_width, OrderMax, bar_width,
alpha=opacity,
# color='g',
label='order max')

rects3 = plt.bar(index + bar_width*2, CouchPeerMax, bar_width,
alpha=opacity,
# color='g',
label='couchDB max')

rects4 = plt.bar(index + bar_width*3, CAMax, bar_width,
alpha=opacity,
# color='g',
label='CA max')

rects1_ = plt.bar(index, PeerAvg, bar_width,
alpha=opacity,
# color='b',
label='peer avg')

rects2_ = plt.bar(index + bar_width, OrderAvg, bar_width,
alpha=opacity,
# color='g',
label='order avg')

rects3_ = plt.bar(index + bar_width*2, CouchPeerAvg, bar_width,
alpha=opacity,
# color='g',
label='couchDB avg')

rects4_ = plt.bar(index + bar_width*3, CAAvg, bar_width,
alpha=opacity,
# color='g',
label='CA avg')

# plt.xlabel('M')
plt.ylabel('Memory (MB)')
# plt.title('Scores by person')
plt.xticks(index + bar_width, ('QueryAll', 'QueryOwn', 'Transfer', 'Issue'))
plt.legend()

plt.tight_layout()
plt.show()
