## generate failure rate of transfer licenses run:
## pip install matplot
## python chart.py

import matplotlib.pyplot as plt
import numpy as np

x = np.array([7.0, 8.0, 9.0, 10.1, 15.1, 20.1, 25.1])
y = np.array([0, 60, 489, 454, 505, 565, 665])

plt.plot(x, y, '.-')
plt.xlabel('Send Rate (tps)')
plt.ylabel('Number of Failure Per 1000 Transaction')

plt.show()
